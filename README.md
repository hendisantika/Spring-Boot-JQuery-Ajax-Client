# Spring Boot JQuery Ajax Client

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

List Users Page

![List Users Page](img/list.png "List Users Page")