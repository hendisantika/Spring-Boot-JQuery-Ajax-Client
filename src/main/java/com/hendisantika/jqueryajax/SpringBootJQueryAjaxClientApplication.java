package com.hendisantika.jqueryajax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJQueryAjaxClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJQueryAjaxClientApplication.class, args);
	}
}
